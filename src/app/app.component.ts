import {Component, OnInit } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {ResizeEvent} from 'angular-resizable-element';

interface IDomNode {
    tag: string | undefined;
    attributes: {
        [key: string]: string
    };
    content: IDomNode[];
    text?: string;
}
var testNode: IDomNode = {
        tag:"div", 
        attributes:{"margin":"5px","font":"Verdana", "width":"100px"},
        content:[{
            tag:"span", 
            attributes:{"color":"yellow","font-size":"20px"},
            content:[],
            text:"вложенный 1"
        }],
        text:"тестовый элемент"
    }

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

    

export class AppComponent implements OnInit{
    htmlToAdd: HTMLElement;
    element : string;
    appTitle: string = 'Тестовая задача';
    btnTitle:string  = 'нажми меня';
    tag : string ='div';
    redBool = true;
    greenBool = false;
    blueBool = false;
    
    constructor(private http: HttpClient) {
        //this.createComp(testNode);
    }
    
    onResizeEnd(event: ResizeEvent):void{
        console.log('Element was resized', event);
    }
    
    ngOnInit(): void {
        this.createComp(testNode);
        this.http.get('/startData.json').subscribe(data=>{
            // обработка запроса
        }, (err:HttpErrorResponse) => {
            if (err.error instanceof Error) {
                console.log("Client-side Error occured");
            } else {
                console.log("Server-side Error occured");
            }
        }
        )
    }
    
    createComp(node: IDomNode): void{
        var keys: string[] =  Object.keys(node.attributes);
        var attr:string;
        for (var i in keys){
            attr+=keys[i]+": ";
            attr += node.attributes[keys[i]]+"; ";
        }
        this.element = "<" + node.tag + " style=\"" + attr + "\">"+node.text+"</"+node.tag+">";
        
    }
    
    getStyle(){
        var myStyles= {
            'color':"red"
        };
        return myStyles;
    }
    
    
    
    createHtml():void {
        
    }
    
    changeColor(){
        if(this.redBool){
            this.redBool = false;
            this.greenBool  =true;
        } else if (this.greenBool){
            this.greenBool  =false;
            this.blueBool = true;
        } else if (this.blueBool) {
            this.blueBool = false;
            this.redBool = true;
        }
    }
}