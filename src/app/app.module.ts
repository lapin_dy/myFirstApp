import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ResizableModule} from 'angular-resizable-element';
import { AppComponent } from './app.component';

import {Interceptor} from './Interceptor';
import { NewComponentComponent } from './new-component/new-component.component';



@NgModule({
  declarations: [
    AppComponent,
    NewComponentComponent
  ],
  imports: [
    BrowserModule,
    ResizableModule,
    HttpClientModule,
    FormsModule, ReactiveFormsModule
  ],
  providers: [{
      provide:HTTP_INTERCEPTORS,
      useClass: Interceptor,
      multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
